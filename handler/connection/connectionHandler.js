module.exports = (io, socket) => {
	// Je recois les informations de connection de l'utilisateur au socket
	socket.on("connection:join", async ({ token }) => {
		// Identifie l'utilisateur a partir de son Token Sanctum
		fetch(process.env.BACK_URL + "/auth/me", {
			method: "POST",
			headers: {
				Authorization: "Bearer " + token,
				Accept: "application/json",
				"Content-type": "application/json",
				"Access-Control-Allow-Origin": "*",
				"Access-Control-Request-Method": "GET, POST, PUT, OPTIONS",
			},
		}).then(async (response) => {
			let user = await response.json();

			// L'utilisateur à donné le bon token
			if (user.id) {
				// Enregistrement de l'utilisateur dans le redis
				await redisClient.set("token/" + token, JSON.stringify(user));

				// La connexion c'est bien passé
				socket.emit("connection:join:response", true);
			}
			// Le token n'est pas bon
			else {
				socket.emit("connection:join:response", false);
			}
		});
	});

	// Un utilisateur vient de se deconncter du serveur webSocket
	socket.on("disconnect", () => {
		// Affichage message d'information en mode dev
		if (process.env.ENV == "dev") console.log("a user disconnected");
	});
};
